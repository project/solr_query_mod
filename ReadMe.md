#Solr Query Modification

This module rewrites the solr query from Search API Solr to add wildcards and make the search fuzzy. How to modify the query can be controlled from an admin page.

To use the module you install and enable it, go to the admin page and select the modifications you want for your search.

Beware that it may conflict with other hook_search_api_solr_query_alter implementations.

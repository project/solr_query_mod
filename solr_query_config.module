<?php
/**
 * @file
 * Module file for solr_query_config
 */

/**
 * Implements hook_menu()
 */
function solr_query_config_menu() {
  $items = array();

  $items['admin/config/search/solr-query-config'] = array(
    'title' => 'Solr Query configuration admin',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('_solr_query_config_administration'),
    'access arguments' => array('manage solr query config'),
    'type' => MENU_NORMAL_ITEM,
    'file' => 'solr_query_config.admin.inc',
  );
  return $items;
}

/**
 * Implements hook_permission()
 *
 * Add permission to manage query configuration.
 */
function solr_query_config_permission() {
  $permissions = array();

  $permissions['manage solr query config'] = array(
    'title' => t('Manage Solr search query configuration'),
    'description' => t('Enable and disable modifications.'),
    'warning' => t('Improper handling of the settings will produce weird search results.'),
  );

  return $permissions;
}

/**
 * Implements hook_search_api_solr_query_alter().
 */
function solr_query_config_search_api_solr_query_alter(array &$call_args, SearchApiQueryInterface $query) {

  // Modify the search. Split the search and create a solr query.
  $config_settings = variable_get('solr_query_config_config', array());
  if (!empty($call_args['query']) && !empty($config_settings['solr_query_config_enable'])) {
    $call_args['query'] = _solr_query_config_modify_query($call_args['query'], $config_settings);

    // If the query only consisted of unwanted symbols.
    if (empty($call_args['query'])) {
      $call_args['params']['rows'] = 0;
    }
  }
}

/**
 * Tear apart the query and reassemble with configuration.
 *
 * @param string $query_string
 *   A query string from search_api_solr
 * @param array $settings
 *   An array of settings from the admin form.
 *
 * @return string
 *   The modified search string.
 */
function _solr_query_config_modify_query($query_string_raw, $settings = array()) {
    // Clear the string for unwanted characters.
    $query_string = trim(str_replace(['"', ':', '(', ')', '^', '~', '*', '?', '%', '{', '}', '[', ']', '!', "'", '\\'], '', $query_string_raw));
    if (empty($query_string)) {
      return '';
    }
    $search = str_replace('-', ' ', $query_string);
    $last_word = '';
    $search_words = [];

    $search_words_raw = explode(' ', $search);
    $last_word = trim(array_pop($search_words_raw));

    $word_count = count($search_words_raw);

    foreach ($search_words_raw as $searchword) {
      _solr_query_config_modify_single_word($searchword, $settings, $search_words);
    }

    if (!empty($settings['solr_query_config_max_word_count']) && !empty($search_words)) {
      $search_words = array_splice($search_words, 0, (intval($settings['solr_query_config_max_word_count']) - 1));
    }


    if (!empty($settings['solr_query_config_wildcard_after_last'])) {
      $settings['solr_query_config_wildcard_after'] = TRUE;
    }
    _solr_query_config_modify_single_word($last_word, $settings, $search_words);


    $query = implode(' AND ', $search_words);
    // If there was originally more than one word in the phrase, we add the
    // complete phrase to the search.
    if (!empty($query)) {
      $query = '"' . $query_string . '" OR (' . $query . ')';
    }
    else {
      $query = '"' . $query_string . '"';
    }

    return $query;

}

/**
 * Create the query part for a single word based on the settings.
 *
 * @param string $word
 *   The word to add to the query.
 * @param array $settings
 *   Settings as saved from the admin page.
 * @param array $search_words.
 *   The array of query parts to implode later.
 */
function _solr_query_config_modify_single_word($word, $settings, &$search_words) {
  if (empty($word) || !trim($word)) {
    return;
  }

  if (!empty($settings['solr_query_config_minimum_length']) && strlen($word) < (int) $settings['solr_query_config_minimum_length']) {
    return;
  }
  $search = array($word);
  if (!empty($settings['solr_query_config_fuzzy'])) {
    $search[] = $word . '~' . $settings['solr_query_config_fuzzy_sharpness'];
  }

  if (!empty($settings['solr_query_config_wildcard_after'])) {
    $search[] = $word . '*';
  }

  if (!empty($settings['solr_query_config_wildcard_both'])) {
    $search[] = '*' . $word . '*';
  }

  $search_words[] = '(' . implode(' OR ', $search) . ')';
}

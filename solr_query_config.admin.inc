<?php
/**
 * @file
 * Configuration form for solr_query_config
 */

/**
 * Form callback for configuration page.
 */
function _solr_query_config_administration($form, &$form_state) {
  $settings = variable_get('solr_query_config_config', array());

  $form['solr_query_config_enable'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable query modification'),
    '#description' => t('If not enabled, everything below will be ignored.'),
    '#default_value' => !empty($settings['solr_query_config_enable']),
  );

  $form['solr_query_config_fuzzy'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable fuzzy search'),
    '#default_value' => !empty($settings['solr_query_config_fuzzy']),
  );

  $options = array(
    '0.2' => '0.2 ' . t('Very fuzzy'),
    '0.3' => '0.3',
    '0.4' => '0.4',
    '0.5' => '0.5',
    '0.6' => '0.6',
    '0.7' => '0.7',
    '0.8' => '0.8',
    '0.9' => '0.9 ' . t('Not that fuzzy'),
  );
  $form['solr_query_config_fuzzy_sharpness'] = array(
    '#type' => 'select',
    '#title' => t('Select fuzzy sharpness'),
    '#options' => $options,
    '#description' => t('Only relevant if fuzzy is enabled'),
  );

  if (!empty($settings['solr_query_config_fuzzy_sharpness'])) {
    $form['solr_query_config_fuzzy_sharpness']['#default_value'] = $settings['solr_query_config_fuzzy_sharpness'];
  }

  $form['solr_query_config_wildcard_after_last'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable wildcard after last word'),
    '#description' => t('Not relevant if wildcard is applied after all words.'),
    '#default_value' => !empty($settings['solr_query_config_wildcard_after_last']),
  );

  $form['solr_query_config_wildcard_after'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable wildcard after each word'),
    '#default_value' => !empty($settings['solr_query_config_wildcard_after']),
  );

  $form['solr_query_config_wildcard_both'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable wildcard before and after each word'),
    '#default_value' => !empty($settings['solr_query_config_wildcard_both']),
  );

  $form['solr_query_config_minimum_length'] = array(
    '#type' => 'textfield',
    '#title' => t('Minimum word length for fuzzy and wildcard searches.'),
    '#default_value' => !empty($settings['solr_query_config_minimum_length']) ? $settings['solr_query_config_minimum_length'] : '',
    '#size' => 2,
  );

  $form['solr_query_config_max_word_count'] = array(
    '#type' => 'textfield',
    '#title' => t('Maximum number of words.'),
    '#description' => t('This module can produce quite long queries, so a limitation of words is recommended. Set to 0 for no limit.'),
    '#default_value' => !empty($settings['solr_query_config_max_word_count']) ? $settings['solr_query_config_max_word_count'] : 6,
    '#size' => 2,
  );

  $form['solr_query_config_submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save settings'),
  );
  $form['#submit'][] = '_solr_query_config_administration_submit';

  $form['query_test'] = array(
    '#type' => 'fieldset',
    '#title' => t('Test settings'),
  );
  $form['query_test']['search_dummy'] = array(
    '#type' => 'textfield',
    '#title' => t('Search example'),
    '#description' => t('Type an example search to see what the query will look like. You do not need to save.'),
    '#suffix' => '<div id="solr-query-config"></div>',
  );
  $form['query_test']['submit_dummy'] = array(
    '#type' => 'submit',
    '#value' => t('Show query'),
    '#ajax' => array(
      'wrapper' => 'solr-query-config',
      'callback' => '_solr_query_config_test_query',
    ),
  );

  return $form;
}

function _solr_query_config_test_query($form, &$form_state) {
  $test_settings = array();
  $fields = array(
    'solr_query_config_enable',
    'solr_query_config_fuzzy',
    'solr_query_config_fuzzy_sharpness',
    'solr_query_config_wildcard_after_last',
    'solr_query_config_wildcard_after',
    'solr_query_config_wildcard_both',
    'solr_query_config_minimum_length',
    'solr_query_config_max_word_count',
  );
  foreach ($fields as $field) {
    $test_settings[$field] = $form_state['values'][$field];
  }

  $query = _solr_query_config_modify_query($form_state['values']['search_dummy'], $test_settings);
  return '<div id="solr-query-config">' . $query . '</div>';
}

/**
 * Submit handler for the admin form.
 */
function _solr_query_config_administration_submit($form, &$form_state) {
  $fields = array(
    'solr_query_config_enable',
    'solr_query_config_fuzzy',
    'solr_query_config_fuzzy_sharpness',
    'solr_query_config_wildcard_after_last',
    'solr_query_config_wildcard_after',
    'solr_query_config_wildcard_both',
    'solr_query_config_minimum_length',
    'solr_query_config_max_word_count',
  );
  $settings = variable_get('solr_query_config_config', array());
  foreach ($fields as $field) {
    $settings[$field] = $form_state['values'][$field];
  }

  variable_set('solr_query_config_config', $settings);
}
